using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dictionary {

    Hashtable words = new Hashtable();

    public void InsertWord (Word word) {
        words[word.Name] = word;
    }

    public Word Populate (string name, string description, Action action) {
		Word word = new Word(name, description, action);
		InsertWord(word);
		return word;
	}

	public Word Populate (string name, string description) {
		Word word = new Word(name, description);
		InsertWord(word);
		return word;
	}

    public Word FindWord (string name) {
        return words[name] as Word;
    }

    public Word LiteralWord (string name) {
        string unPrefix = Word.UnPrefix(name);

        Word find = words[unPrefix] as Word;

        if (find == null) {
            find = new Word(unPrefix);
            InsertWord(find);
        }

        Word quotedWord = new Word(name);
        quotedWord.Value = find;
        InsertWord(quotedWord);

        return quotedWord;
    }

	public Word[] AllWords () {
		ICollection col = words.Values;
		Word[] arr = new Word[col.Count];
		col.CopyTo (arr, 0);
		return arr;
	}

}
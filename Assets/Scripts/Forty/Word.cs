using System.Collections.Generic;

public class Word {
    string name;
    string description;
    LinkedList<object> values = new LinkedList<object>();

    public string Name {
        get { return name; }
    }

    public string Description {
        get { return description; }
        set { description = value; }
    }

    public object Value {
        get { return values.First.Value; }
        set { this.values.AddFirst(value); }
    }

    public Word (
        string name,
        string description = "No description provided",
        object value = null
        )
    {
        this.name = name;
        this.description = description;
        if (value != null) {
            this.values.AddFirst(value);
        } else {
            values.AddFirst(this);
        }
    }

    public void Forget () {
        values.RemoveFirst();
        if (values.Count == 0) {
            values.AddFirst(this); // 'nothing is better ?
        }
    }


    override
    public string ToString()
    {
        return name;
    }


    static public bool IsLiteral (string name) {
        return name[0] == '\'';
    }

    static public bool IsComposition (string name) {
        return name[0] == '(' && name[name.Length - 1] == ')';
    }

    static public bool IsString (string name) {
        return name[0] == '"' && name[name.Length - 1] == '"';
    }

    static public bool IsMessage (string name) {
        return name[0] == '~' && name[name.Length - 1] == '~';
    }

    static public string UnPrefix (string name) {
        return name.Substring(1);
    }

    static public string UnString (string name) {
        return name.Substring(1, name.Length - 2);
    }
}
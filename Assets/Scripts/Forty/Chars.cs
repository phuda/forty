public class Chars {
	public const int greenColor = -4;
	public const int redColor = -3;
	public const int yellowColor = -2;
	public const int whiteColor = -1;
	public const int endOfFile = 3;
    public const int backSpace = 8;
	public const int tab = 9;
    public const int lineFeed = 10;
    public const int carretReturn = 13;
    public const int whiteSpace = 32;
    public const int romb = 128;

	public const int upArrow = 501;
	public const int downArrow = 502;
	public const int leftArrow = 503;
	public const int rightArrow = 504;
}
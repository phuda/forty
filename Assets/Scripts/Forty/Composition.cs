using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

class Composition {

	Processor processor;
    ArrayList things;

    public ArrayList Things {
        get { return things; }
    }

	public Composition (Processor processor, object[] things) {
		this.processor = processor;
        this.things = new ArrayList(things);
    }

    public void Do () {
		// do it from end, so fisrt item will lay at the top of execution stack
		for (int i = things.Count - 1; i >= 0; i--) {
			object thing = things [i];
            if (thing is Word) {
				processor.ThingsToDo.Push (thing as Word);
            } else {
				processor.ThingsToDo.Push (thing); // literal or composition
            }
        }
    }

    override
    public string ToString() {
        string outString = "( ";
        foreach (object thing in things) {
            string s;
            if (thing is float) {
                s = (((float) thing).ToString("G", CultureInfo.InvariantCulture));
            } else {
                s = thing.ToString ();
            }
            outString += s + " ";
        }
        outString = outString + ")";
        return outString;
    }
}
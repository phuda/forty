﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.Globalization;

public class Processor : MonoBehaviour
{

	public int hertz = 120;
	public bool preLoad = false;

	[Header("Iterfaces")]
	public Screen screen;
	public Keyboard keyboard;
	public AudioSource beep;
	public AudioSource tick;
	public Wireless wireless;
	public Transform meter;

	[Header("Preloads")]
	public List<TextAsset> includes;
	[Multiline]
	public string afterAll;

	// Termainal input handling
	char[] spaces = new char[]{
		(char) Chars.whiteSpace, (char) Chars.tab,
		(char) Chars.lineFeed, (char) Chars.carretReturn
	};
	char[] inputBuffer = new char[128];
	int inputBufferPointer = 0;
	ArrayList history = new ArrayList(32);

	// Processing actually
	Dictionary dict = new Dictionary();
	Stack stack = new Stack(64);
	Stack thingsToDo = new Stack(64); // a buffer for lock-free execution

	// we need more execution stacks: jobs, to do parallel things

	public Dictionary Dictionary {
		get { return dict; }
	}

	public Stack Stack {
		get { return stack; }
	}

	public Stack ThingsToDo {
		get { return thingsToDo; }
	}

	void Awake ()
	{
		if (keyboard != null)
			keyboard.Connect (this);
		if (wireless != null)
			wireless.Connect (this);
	}

	void Reset ()
	{
		stack.Clear ();
		thingsToDo.Clear ();
		inputBufferPointer = 0;
		if (screen != null) {
			screen.Clear ();
			screen.Cursor ();
		}
	}

	public void Exception(string message) {
		if (screen != null) {
			screen.Put (Chars.yellowColor);
			screen.PrintLine (message);
			screen.Put (Chars.whiteColor);
		}
		throw new Exception(message);
	}

	void CheckStack () {
		if (stack.Count == 0) {
			Exception ("Stack underflow!");
		}
	}

	object Pop () {
		CheckStack ();
		return stack.Pop ();
	}

	object Peek () {
		CheckStack ();
		return stack.Peek ();
	}

	void Push (object thing) {
		stack.Push (thing);
	}

	Word PopWord () {
		object thing = Pop ();
		if (!(thing is Word)) Exception (thing.ToString() + " is not Word!");
		return thing as Word;
	}

	float PopNumber () {
		object thing = Pop ();
		if (!(thing is float)) Exception (thing.ToString() + " is not Number!");
		return (float) thing;
	}

	string PopString () {
		object thing = Pop ();
		if (!(thing is string)) Exception (thing.ToString() + " is not String!");
		return thing as string;
	}

	Composition PopComp () {
		object thing = Pop ();
		if (!(thing is Composition)) Exception (thing.ToString() + " is not Composition!");
		return thing as Composition;
	}

	void Start ()
	{
		Word wNothing = dict.Populate("nothing",
			"NO FU**ING THING!");

		//////////////////////////////////////////  LOGIC
		Word wTrue = dict.Populate("true",
			"Logical true");
		Word wFalse = dict.Populate("false",
			"Logical false");
		dict.Populate("equal?",
			"( o o -- b )",
			() => {
				object b = Pop(), a = Pop();
				bool e = a is float && b is float && (float) a == (float) b ? true : a == b ? true : false;
				Push(e ? wTrue : wFalse);
			});
		dict.Populate("and",
			"( b b -- b )\n" +
			"Logical and",
			() => Push(Pop () == wTrue && Pop () == wTrue ? wTrue : wFalse));
		dict.Populate("or",
			"( b b -- b )\n" +
			"Logical or",
			() => Push(Pop () == wTrue || Pop () == wTrue ? wTrue : wFalse));
		dict.Populate("xor",
			"( b b -- b )\n" +
			"Logical xor",
			() => Push(Pop () == wTrue ^ Pop () == wTrue ? wTrue : wFalse));

		//////////////////////////////////////////  WORD BINDING
		Action aSet = () => PopWord().Value = Pop ();
		dict.Populate("set",
			"( o w -- )\n" +
			"Bind object to word",
			aSet);
		dict.Populate("!",
			"( o w -- )\n" +
			"Short for SET",
			aSet);

		Action aGet = () => Push(PopWord().Value);
		dict.Populate("get",
			"( w -- o )\n" +
			"Get object binded to word",
			aGet);
		dict.Populate("@",
			"( w -- o )\n" +
			"Short for GET",
			aGet);

		Action aForget = () => PopWord().Forget();
		dict.Populate("forget",
			"( w -- )\n" +
			"Forget last bind",
			aForget);
		dict.Populate(":",
			"( w -- )\n" +
			"Short for FORGET",
			aForget);

		Action aSetDoc = () => PopWord().Description = PopString();
		dict.Populate("set-doc",
			"( s w -- )\n" +
			"Bind object to word",
			aSetDoc);
		dict.Populate("doc!",
			"( s w -- )\n" +
			"Short for DOC",
			aSetDoc);

		dict.Populate ("words",
			"( -- (...) )\n" +
			"Composition with all words avaliable to the system",
			() => Push(new Composition (this, dict.AllWords ())));

		//////////////////////////////////////////  COMPOSITION
		dict.Populate("push",
			"( (...) o -- (... o) )\n" +
			"Push x to composition",
			() => {
				object thing = Pop();
				ArrayList things = PopComp().Things.Clone() as ArrayList;
				things.Add(thing);
				Push(new Composition(this, things.ToArray() ));
			});

		dict.Populate("length",
			"( (...) -- i )\n" +
			"Length of composition",
			() => {
				ArrayList things = PopComp().Things;
				Push((float) things.Count);
			});

		dict.Populate("nth",
			"( (o ...) i  -- o )\n" +
			"Get element of composition by index, first element has index 0",
			() => {
				int nth = (int) PopNumber();
				ArrayList things = PopComp().Things;
				if (things.Count < nth + 1) {
					Exception("Out of index!");
				}
				Push(things[nth]);
			});

		dict.Populate("rest",
			"( (o ...)  -- (...) )\n" +
			"Get all but first elements of composition",
			() => {
				ArrayList things = PopComp().Things.Clone() as ArrayList;
				if (things.Count > 0) {
					things.RemoveAt(0);
				}
				Push(new Composition(this, things.ToArray()));
			});

		//////////////////////////////////////////  COMPOSITION GENERATORS
		dict.Populate("decomp",
			"( (a b ... c) -- a b ... c )\n" +
			"",
			() => {
				ArrayList comp = PopComp().Things;
				for (int i = 0; i < comp.Count; i++) {
					Push(comp[i]);
				}
			});

		dict.Populate("comp",
			"( a b ... c i -- (a b ... c) )\n" +
			"Create composition with items from stack",
			() => {
				int count = (int) PopNumber();
				object[] comp = new object[count];
				for (int i = count - 1; i >= 0; i--) {
					comp[i] = Pop();
				}
				Push (new Composition(this, comp));
			});

		dict.Populate("repeat",
			"( i o -- (o o ...) )\n" +
			"Repeat thing several times",
			() => {
				object thing = Pop();
				int count = (int) PopNumber();
				object[] toRepeat = new object[count];
				for (int i = 0; i < count; i++) {
					toRepeat[i] = thing;
				}
				Push (new Composition(this, toRepeat));
			});

		dict.Populate("range",
			"( i-from i-to i-step -- )\n" +
			"Create composition with range of numbers",
			() => {
				int step = (int) PopNumber(), to = (int) PopNumber(), from = (int) PopNumber();
				List<object> nums = new List<object>();
				if (step > 0 && from < to) {
					for (int i = from; i < to; i += step) {
						nums.Add(i);
					}
				} else if (step < 0 && from > to) {
					for (int i = from; i > to; i += step) {
						nums.Add(i);
					}
				}
				Push (new Composition(this, nums.ToArray()));
			});

		//////////////////////////////////////////  COMBINATORS
		dict.Populate("do",
			"( (...) -- )\n" +
			"Do composition",
			() => PopComp().Do());

		dict.Populate("map",
			"( (...) (...) -- (...) )",
			() => {

			});

		dict.Populate("if",
			"( b (...)-then (...)-else -- )",
			() => {
				Composition compElse = PopComp();
				Composition compThen = PopComp();
				object pred = Pop();
				if (pred != wFalse && pred != wNothing || pred is int && (int) pred != 0) {
					compThen.Do();
				} else {
					compElse.Do();
				}
			});

		//////////////////////////////////////////  SCREEN
		if (screen != null) {

			dict.Populate("clear",
				"Clear screen",
				() => screen.Clear());
			dict.Populate("emit",
				"Print character by number",
				() => screen.Put ((int) PopNumber()));

			dict.Populate("print",
				"( o -- )\n" +
				"Print object from stack",
				() => {
					object o = Pop();
					if (o is float) {
						screen.PrintString (((float) o).ToString("G", CultureInfo.InvariantCulture));
					} else {
						screen.PrintString (o.ToString ());
					}
				});
		}

		//////////////////////////////////////////  STACK OPERATIONS
		// http://wiki.laptop.org/go/Forth_stack_operators
		dict.Populate("dup",
			"( o -- o o )\n" +
			"Copy object on stack",
			() => Push(Peek()));
		dict.Populate("drop",
			"( o -- )\n" +
			"Drop object from stack",
			() => Pop());
		dict.Populate("swap",
			"( o-a o-b -- o-b o-a )\n" +
			"Swap two objects on stack",
			() => {
				object b = Pop(), a = Pop();
				Push(b); Push(a);
			});
		dict.Populate("over",
			"( o-a o-b -- o-a o-b o-a )\n" +
			"Copy object over",
			() => {
				object b = Pop(), a = Pop();
				Push(a); Push(b); Push(a);
			});
		dict.Populate("rot",
			"( o-a o-b o-c -- o-b o-c o-a )\n" +
			"Swap two elements on stack",
			() => {
				object c = Pop(), b = Pop(), a = Pop();
				Push(b); Push(c); Push(a);
			});
		dict.Populate("stack",
			"( -- (...) )\n" +
			"All objects from stack",
			() => {
				List<object> list = new List<object>(stack.ToArray ());
				list.Reverse();
				Push(new Composition(this, list.ToArray()));
			});
		dict.Populate("clear-stack",
			"( ... -- )\n" +
			"Clear stack",
			() => stack.Clear());

		//////////////////////////////////////////  PROCESSOR
		dict.Populate("random",
			"( -- n )\n" +
			"Random number between 0.0 and 1.0",
			() => Push(UnityEngine.Random.value));
		dict.Populate("reset",
			"Reset",
			() => Reset());
		dict.Populate("nop",
			"Do nothing",
			() => {});

		//////////////////////////////////////////  MATH
		dict.Populate("+",
			"( n n -- n )\n" +
			"Sum of two numbers",
			() => Push (PopNumber() + PopNumber()));
		dict.Populate("-",
			"( n n -- n )\n" +
			"Dist of two numbers",
			() => {
				float b = PopNumber(), a = PopNumber();
				Push (a - b);
			});
		dict.Populate("*",
			"( n n -- n )\n" +
			"Mul of two numbers",
			() => Push (PopNumber() * PopNumber()));
		dict.Populate("/",
			"( n n -- n )\n" +
			"Div of two numbers",
			() => {
				float b = PopNumber(), a = PopNumber();
				Push (a / b);
			});
		dict.Populate("mod",
			"( n n -- n )\n" +
			"Mod of the numbers",
			() => {
				float b = PopNumber(), a = PopNumber();
				Push (a % b);
			});

		//////////////////////////////////////////  NETWORK
		dict.Populate("broadcast",
			"( s -- )\n" +
			"Send command to all visible copmuters",
			() => {
				if (wireless == null) Exception("No connection!");
				wireless.Broadcast(PopString());
			});
		dict.Populate("scan",
			"( -- (...) )\n" +
			"All visible copmuters",
			() => {
				if (wireless == null) Exception("No connection!");
				Push(new Composition(this, wireless.Scan().ToArray()));
			});

		//////////////////////////////////////////  ACTUATORS
		if (beep != null) {
			dict.Populate("beep", // can be done as terminal signal
				"Signal a sound",
				() => beep.Play());
		}

		//////////////////////////////////////////  HELP
		dict.Populate("description",
			"(w -- word-description)",
			() => Push(PopWord().Description));

		StringBuilder sb = new StringBuilder (1024);

		foreach (TextAsset t in includes) {
			sb.Append (t.text);
			sb.Append ((char) Chars.carretReturn);
		}

		sb.Append (afterAll);

		ReadLine (sb.ToString()).Do ();

		// if (preLoad) {
		// 	while (thingsToDo.Count != 0) {
		// 		Process();
		// 	}
		// }

		StartCoroutine(Tick());
	}

	void Process () {
		if (thingsToDo.Count == 0) return;

		object thing = thingsToDo.Pop();
		if (thing is Word) {
			object value = (thing as Word).Value;
			if (value is Action) {
				try {
					(value as Action).Invoke ();
				} catch (Exception e) {
					Debug.LogWarning(e.Message);
					thingsToDo.Clear();
				}
			} else if (value is Composition) {
				(value as Composition).Do ();
			} else {
				stack.Push (value);
			}
		} else {
			stack.Push (thing); // literal or composition
		}
	}

	IEnumerator Tick () {
		while (true) {
			float tickTime = 1f / hertz;
			if (tick != null) {
				tick.Play();
			}
			if (thingsToDo.Count > 0) {
				for (int i = 0; i < Math.Ceiling(Time.deltaTime / tickTime); i++) {
					Process();
				}
				yield return new WaitForSeconds(tickTime);
				continue;
			}
			yield return new WaitForSeconds(tickTime * 5);
		}
	}

	void Update ()
	{
		if (meter != null) {
			meter.localScale = new Vector3(Math.Min(thingsToDo.Count / 32f, 1f), 1, 1);
		}
	}

	string InputBufferToString ()
	{
		string outString = new string (inputBuffer, 0, inputBufferPointer);
		inputBufferPointer = 0;
		return outString;
	}

	object Parse (string name) {
		Word word = dict.FindWord(name);

		if (word != null) {
			return word;
		} else if (Word.IsLiteral(name)) {
			return dict.LiteralWord(name);
		} else if (Word.IsComposition(name)) {
			return ReadLine(Word.UnString(name));
		} else if (Word.IsString(name) || Word.IsMessage(name)) {
			return Word.UnString(name);
		} else {
			try {
				return float.Parse(name, CultureInfo.InvariantCulture.NumberFormat);
			} catch (System.Exception) {
				return null;
			}
		}
	}

	List<string> Split (string line) {
		List<string> names = new List<string>();

		if (line.Length == 0) return names;

		int i = 0;

		do {
			// TODO: Refactor this shit
			char c = line[i];
			if (c == '"') {
				int close = line.IndexOf('"', i + 1);
				if (close > 0) {
					names.Add(line.Substring(i, close - i + 1));
					i = close + 1;
				} else {
					Exception("String is not closed!");
					break;
				}
				continue;
			} else if (c == '~') {
				int close = line.IndexOf('~', i + 1);
				if (close > 0) {
					names.Add(line.Substring(i, close - i + 1));
					i = close + 1;
				} else {
					Exception("Message is not closed!");
					break;
				}
				continue;
			} else if (c == '(') {
				int level = 1;
				int j = i + 1;
				do {
					char check = line[j];
					level += check == '(' ? 1 : check == ')' ? -1 : 0;
					if (level == 0) {
						names.Add(line.Substring(i, j - i + 1));
						break;
					}
					j++;
				} while (j < line.Length);
				i = j + 1;
				continue;
			} else if (c > 32 && c < 127) {
				int close = line.IndexOfAny(spaces, i + 1);
				if (close > 0) {
					names.Add(line.Substring(i, close - i));
					i = close + 1;
				} else {
					names.Add(line.Substring(i, line.Length - i));
					break;
				}
				continue;
			}

			i++;
		} while (i < line.Length);

		return names;
	}

	Composition ReadLine (string input) {
		// Debug.Log ("To read: " + input);

		List<string> line = Split (input);
		List<object> things = new List<object>();

		foreach (string name in line) {
			object thing = Parse (name);
			if (thing != null) {
				things.Add(thing);
			} else {
				if (screen != null) {
					screen.Put (Chars.lineFeed);
					screen.PrintLine (name + " ?");
				}
				return null;
			}
		}
		return new Composition(this, things.ToArray());
	}

	int historyLevel = 0;

	public void InputString (string s, bool echo = true)
	{
		foreach (char c in s) {
			Input(c, echo);
		}
		Input(Chars.carretReturn, echo);
	}

	public void Input (int c, bool echo = true)
	{
		switch (c) {
		case Chars.endOfFile:
			Reset ();
			break;
		case Chars.lineFeed:
		case Chars.carretReturn:
			historyLevel = 0;
			string input = InputBufferToString ();
			if (input != "") {
				history.Add (input);
			}
			Composition toDo = ReadLine(input);
			if (toDo != null) {
				toDo.Do ();
				//  ok
			}
			break;
		case Chars.backSpace:
			inputBufferPointer = Mathf.Max(0, inputBufferPointer - 1);
			break;
		case Chars.upArrow:
		case Chars.downArrow:
			if (!(historyLevel > 0 && historyLevel < history.Count)) {
				historyLevel = 0;
			}
			string h = history [history.Count - 1 - historyLevel] as string;
			inputBufferPointer = h.Length;
			h.CopyTo (0, inputBuffer, 0, inputBufferPointer);
			if (screen != null) {
				screen.Clear ();
				screen.PrintString (h);
			}
			historyLevel += c == Chars.upArrow ? 1 : -1;
			break;
		default:
			inputBuffer [inputBufferPointer] = (char)c;
			inputBufferPointer = (inputBufferPointer + 1) % inputBuffer.Length;
			break;
		}

		if (echo && screen != null) {
			screen.Put (c);
		}
	}
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorButton : MonoBehaviour {

	public Processor processor;

	public Vector3 colliderCenter;
	public Vector3 colliderSize;

	Animator animator;
	bool pushed = false;


	// Use this for initialization
	void Start () {
		animator = transform.parent.GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		if (Time.frameCount % 15 == 0) {
			Collider[] hitColliders = Physics.OverlapBox (transform.position + colliderCenter, colliderSize);
			if (!pushed && hitColliders.Length > 0) {
				pushed = true;
				animator.Play ("Push");
				processor.InputString ("on");
			} else if (pushed && hitColliders.Length == 0) {
				pushed = false;
				animator.Play ("Off");
				processor.InputString ("off");
			}
		}
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube (transform.position + colliderCenter, colliderSize * 2);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGeneratorAdapter : MonoBehaviour {

	public ItemGenerator itemGenerator;

	void Start () {
		Processor p = GetComponent<Processor>();

		p.Dictionary.Populate("copy",
			"Copy something",
            () => {
				if (itemGenerator.IsReady()) {
					bool suc = itemGenerator.Copy();
					if (!suc) {
						p.Exception("Nothing to copy!");
					}
				} else {
					p.Exception("Reloading copier!");
				}
			});
	}
}

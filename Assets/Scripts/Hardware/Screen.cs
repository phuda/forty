﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screen : MonoBehaviour
{

	public int width = 25;
	public int height = 13;

	int border = 3;

	int length;

	int charWidth = 8;
	int charHeight = 13;

	int charLength;
	int screenLength;
	int screenHeight;

	public Texture2D charset;

	int charsetWidth;
	int charsetHeight;

	int carretX = 0;
	int carretY = 0;

	int color = Chars.whiteColor;

	Color bgColor = new Color (0, 0, 0);

	Texture2D texture;

	bool textureNeedApply;
	bool lastCarretReturn;

	void Awake ()
	{
		Debug.Log ("Screen resolution: " + width * charWidth + " x " + height * charHeight);

		length = width * height;

		charWidth = 8;
		charHeight = 13;

		charLength = charWidth * charHeight;
		screenLength = charLength * length;
		screenHeight = charHeight * (height - 1);

		charsetWidth = charset.width / charWidth;
		charsetHeight = charset.height / charHeight;
		Debug.Log ("Chars count: " + charsetWidth * charsetHeight);

		// texture = new Texture2D (width * charWidth, height * charHeight, TextureFormat.R8, true);
		texture = new Texture2D (width * charWidth + border * 2, height * charHeight + border * 2);
		texture.wrapMode = TextureWrapMode.Clamp;

		Renderer r = GetComponent<Renderer> ();
		r.material.color = Color.white;
		r.material.mainTexture = texture;
		r.material.SetTexture ("_EmissionMap", texture);
		r.material.SetColor ("_EmissionColor", Color.white);

		Clear ();
		// Cursor ();

		texture.Apply ();
	}

	void Start ()
	{

	}

	void Update ()
	{
		if (textureNeedApply) {
			textureNeedApply = false;
			texture.Apply ();
		}
	}

	void BlitChar (int c) {
		int x = c % charsetWidth;
		int y = c / charsetWidth;

		Color[] character =
			charset.GetPixels (
				x * charWidth,
				(charsetHeight - 1 - y) * charHeight,
				charWidth,
				charHeight
			);

		if (color < -1) {
			Color mul = Color.white;
			switch (color) {
			case Chars.redColor:
				mul = Color.red;
				break;
			case Chars.yellowColor:
				mul = Color.yellow;
				break;
			case Chars.greenColor:
				mul = Color.green;
				break;
			}

			for (int i = 0; i < character.Length; i++) {
				character [i] *= mul;
			}
		}

		texture.SetPixels (
			carretX * charWidth + border,
			screenHeight - carretY * charHeight + border,
			charWidth,
			charHeight,
			character
		);
	}

	public void Cursor () {
		BlitChar (160);
	}

	public void Put (int c)
	{
		// fuck Windows, fuck CRLF 
		if (lastCarretReturn) {
			lastCarretReturn = false;
			if (c == Chars.lineFeed) {
				return;
			}
		}

		if (c < 0) {
			color = c;
			return;
		} else if (c > 500) {
			return; // arrow keys
		} else if (c > 255) {
			c = Chars.romb; // unknow symbol
		}

		switch (c) {
		case Chars.backSpace:
			BlitChar (Chars.whiteSpace); // clear cursor
			carretX -= 1;
			break;
		case Chars.carretReturn:
			lastCarretReturn = true;
			goto case Chars.lineFeed;
		case Chars.lineFeed:
			BlitChar (Chars.whiteSpace); // clear cursor
			carretX = 0;
			carretY++;
			break;
		default:
			BlitChar (c);
			carretX++;
			break;
		}

		// line wrap
		if (carretX >= width) {
			carretX = 0;
			carretY++;
		} else if (carretX < 0) {
			carretX = 0;
		}

		// overflow - shift lines up
		if (carretY >= height) {
			carretY = height - 1;

			int wholeLine = (width * charWidth + border * 2) * charHeight;

			Color[] colors = texture.GetPixels ();
			for (int i = colors.Length - 1; i >= wholeLine; i--) {
				colors [i] = colors [i - wholeLine];
			}
			for (int i = wholeLine - 1; i >= 0; i--) {
				colors [i] = bgColor;
			}
			texture.SetPixels (colors);
		}

		Cursor ();

		textureNeedApply = true;
	}

	public void PrintString (string s)
	{
		foreach (char c in s) {
			Put (c);
		}
	}

	public void PrintLine (string s)
	{
		PrintString (s);
		Put (Chars.lineFeed);
	}

	public void Clear ()
	{
		carretX = 0;
		carretY = 0;

		Color[] colors = texture.GetPixels ();
		for (int i = 0; i < colors.Length; i++) {
			colors [i] = bgColor;
		}
		texture.SetPixels (colors);

		textureNeedApply = true;
	}
}

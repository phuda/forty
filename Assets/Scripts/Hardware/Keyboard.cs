﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keyboard : MonoBehaviour {

	public Player player;
	public Transform screen; // TODO: temporary shit for eyelevel

	Processor listener;

	public Transform Sceen {
		get { return screen; }
	}

	void Start () {

	}

	void Update () {
		if (player.KeyboardUsed != this) return;

		if (listener == null) return;

		string input = Input.inputString;
		if (input.Length != 0) {
			foreach (char c in input) {
				listener.Input (c);
			}
		} else if (Input.GetKeyDown (KeyCode.Backspace)) {
			listener.Input (Chars.backSpace);
		} else if (Input.GetKeyDown (KeyCode.UpArrow)) {
			listener.Input (Chars.upArrow);
		} else if (Input.GetKeyDown (KeyCode.DownArrow)) {
			listener.Input (Chars.downArrow);
		} else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			listener.Input (Chars.leftArrow);
		} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
			listener.Input (Chars.rightArrow);
		} else if (Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl)) {
			if (Input.GetKeyDown (KeyCode.C)) {
				// TODO: doesnt get there on linux
				listener.Input (Chars.endOfFile);
			}
		}
	}

	public void Connect (Processor p) {
		listener = p;
	}
}

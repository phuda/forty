﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wireless : MonoBehaviour {

	public string address;

	Radar radar;

	Animator animator;

	Processor listener;

	void Start () {
		if (address == "") {
			address = Random.Range(1, 100).ToString();
		}
		radar = transform.GetComponentInChildren<Radar>();
		animator = GetComponent<Animator> ();
	}

	void Update () {

	}

	public void Broadcast (string s) {
		animator.Play ("TXBlink");
		foreach (Wireless w in radar.Detected) {
			if (w != this) {
				w.Accept(s);
			}
		}
	}

	public List<string> Scan () {
		return radar.Detected.ConvertAll(x => x.address);
	}

	public void Accept (string s) {
		animator.Play ("RXBlink");
		if (listener != null) {
			listener.InputString(s, false);
		}
	}

	public void Connect (Processor p) {
		listener = p;
	}
}

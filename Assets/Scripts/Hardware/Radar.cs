﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radar : MonoBehaviour {

	public float radius = 10;

	public GameObject line;

	List<Wireless> detected = new List<Wireless>();

	public List<Wireless> Detected {
		get { return detected; }
	}

	void Update () {
		if (Time.frameCount % 15 == 0) {
			Collider[] hitColliders = Physics.OverlapSphere (transform.position, radius);

			List<Collider> colliders = new List<Collider> (hitColliders);
			colliders.RemoveAll (x => x.GetComponent<Wireless> () == null);
			detected = colliders.ConvertAll (x => x.GetComponent<Wireless> ());
		}

		while (detected.Count > transform.childCount) {
			Instantiate (line, transform);
		}

		foreach (Transform child in transform) {
			child.gameObject.SetActive (false);
		}

		int i = 0;
		foreach (Wireless w in detected) {
			//Debug.DrawLine (transform.position, w.transform.position, Color.blue, Time.deltaTime, false);
			LineRenderer lr = transform.GetChild (i++).GetComponent<LineRenderer> ();
			lr.gameObject.SetActive (true);
			lr.SetPosition (0, transform.position);
			lr.SetPosition (1, w.transform.position);
		}
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere (transform.position, radius);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPitch : MonoBehaviour {

	public float amount;

	// Use this for initialization
	void Start () {
		AudioSource a = GetComponent<AudioSource> ();
		a.pitch = a.pitch + (Random.value * 2 - 1) * amount;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

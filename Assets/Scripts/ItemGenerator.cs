﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGenerator : MonoBehaviour {

	public float prepTimerSet = 3;

	public Vector3 colliderCenter;
	public Vector3 colliderSize;

	Transform outPoint;

	float prepTimer = 0;

	void Start () {
		outPoint = transform.GetChild(0);
	}

	void Update () {
		prepTimer -= Time.deltaTime;
	}

	public bool IsReady () {
		return prepTimer < 0;
	}

	public bool Copy () {
		Collider[] hitColliders = Physics.OverlapBox (transform.position + colliderCenter, colliderSize);
		List<Collider> list = new List<Collider>(hitColliders);
		list.RemoveAll(x => x.tag != "Grabable");
		if (list.Count > 0) {
			GameObject thing = Instantiate(list[0].gameObject, outPoint.position, outPoint.rotation, transform);
			thing.GetComponent<Rigidbody>().AddRelativeForce(0, 0, 10, ForceMode.Impulse);
			prepTimer = prepTimerSet;
			return true;
		}
		return false;
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube (transform.position + colliderCenter, colliderSize * 2);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	const float SPEED = 20;

	public GameObject screenBorder;

	Transform camHeight;
	Transform camHeightTo;
	Quaternion camRotateTo;
	Camera cam;
	Rigidbody body;

	Keyboard keyboardUsed;

	bool needToHandleEscape = false;

	Vector3 xz = new Vector3(1, 0, 1);

	public Keyboard KeyboardUsed {
		get { return keyboardUsed; }
	}

	void Awake () {
		camHeight = transform.Find("CameraHeight");
		camHeightTo = transform.Find("CameraHeightTo");
		cam = FindObjectOfType<Camera>();
		body = GetComponent<Rigidbody>();

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

	void Start () {
		// Cursor.lockState = CursorLockMode.Locked;
	}

	void Update () {
		if (Input.GetMouseButtonDown(0)) {
            Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
		}

		if (Input.GetKeyDown(KeyCode.Tab)) {
			if (keyboardUsed) {
				keyboardUsed = null;
				camHeightTo.position = camHeight.position;

				screenBorder.SetActive(false);
			} else {
				needToHandleEscape = true;
			}
		}
	}

	void FixedUpdate () {
		transform.Rotate(
			0,
			Input.GetAxis("Mouse X"),
			0
		);
		cam.transform.Rotate(
			Input.GetAxis("Mouse Y") * -1,
			0,
			0
		);

		if (keyboardUsed == null) {
			transform.Rotate(
				0,
				Input.GetAxis("HorizontalView"),
				0
			);
			cam.transform.Rotate(
				Input.GetAxis("VerticalView") * -1,
				0,
				0
			);

			float mul =
				Input.GetKey (KeyCode.LeftShift) ||
				Input.GetKey (KeyCode.RightShift)
				? 2f : 1f;

			body.AddRelativeForce(
				Input.GetAxis("Horizontal") * SPEED * mul,
				0,
				Input.GetAxis("Vertical") * SPEED * mul,
				ForceMode.Acceleration
			);
		}

		body.AddForce(Vector3.Scale(body.velocity * -5, xz));

		Vector3 c = cam.transform.position;
		cam.transform.position = Vector3.Lerp(c, camHeightTo.position, 2f * Time.deltaTime);

		if (keyboardUsed) {
			// Smoothly rotate towards the target point.
			Quaternion r = cam.transform.rotation;
			cam.transform.rotation = Quaternion.Slerp(r, camRotateTo, 2f * Time.deltaTime);
			Quaternion rl = cam.transform.localRotation;
			cam.transform.localRotation = new Quaternion(rl.x, 0, 0, 1);
		}
	}

	void OnTriggerStay(Collider other) {
		if (needToHandleEscape && other.name == "KeyboardTrigger") {
			needToHandleEscape = false;
			if (keyboardUsed == null) {
				keyboardUsed = other.transform.parent.GetComponent<Keyboard>();
				Vector3 c = cam.transform.position;
				camHeightTo.position = new Vector3(c.x, keyboardUsed.Sceen.position.y, c.z);
				camRotateTo = Quaternion.LookRotation(keyboardUsed.Sceen.position - camHeightTo.position);

				screenBorder.SetActive(true);
			}
		}
	}
}

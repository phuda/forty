﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grab : MonoBehaviour {

	public Player player;

	Transform targetPoint;
	Transform grabPoint;

	Rigidbody move;
	Vector3 where;

	bool check = false;

	void Start () {
		targetPoint = transform.GetChild (0);
	}

	void Update () {
		if (grabPoint != null) {
			where = targetPoint.position - grabPoint.position;
		}

		if (Input.GetKeyDown(KeyCode.Space)) {
			check = true;
		} else if (Input.GetKeyUp(KeyCode.Space)) {
			check = false;
			move = null;
			grabPoint = null;
		}
	}

	void OnTriggerStay (Collider other) {
		if (check && player.KeyboardUsed == null && other.tag == "Grabable") {
			check = false;
			grabPoint = other.transform.GetChild (0);
			move = other.GetComponent<Rigidbody> ();
		}
	}

	void FixedUpdate () {
		if (move != null) {
			move.velocity = where * 10f;
			move.angularVelocity = move.angularVelocity * 0.1f;
		}
	}
}

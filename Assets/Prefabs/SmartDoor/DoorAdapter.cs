﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAdapter : MonoBehaviour {

	public Animator axis;

	// Use this for initialization
	void Start () {
		Processor p = GetComponent<Processor>();

		p.Dictionary.Populate("open",
			"Open the door",
            () => axis.CrossFade("Open", 0.5f));

		p.Dictionary.Populate("close",
			"Close the door",
			() => axis.CrossFade("Close", 0.5f));
	}

	// Update is called once per frame
	void Update () {

	}
}
